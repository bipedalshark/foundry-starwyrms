# Spaceships and Starwyrms for Foundry VTT
This module extends the dnd5e system for [Foundry Virtual Tabletop](https://foundryvtt.com/) with content from [Spaceships and Starwyrms](https://www.drivethrurpg.com/product/258895/Spaceships-and-Starwyrms-Core-Sourcebook) by [HopePunk Press](https://hopepunkpress.com/).

##### Features
- Equipment from Spaceships and Starwyrms, including weapons, armor, computers, and other sci-fi accoutrements (incomplete)
- Additional damage types (Ballistic, Plasma) and weapon/armor properties (many!)
- Barrier Points implemented via Shield Bracers (automatically responsive to new damage types)
- Significant automation added to the basic functionality of the dnd5e system:
  - Setting of character AC upon equipping armor and shields
  - Stacking of some item types, particularly those that are typically kept in large quantities

### To do:
- Fill out rest of equipment from the Beginner's Kit
- Implement automation for more types of equipment
- Change ammo consumption to occur upon making attack rolls rather than immediately upon activating a Reloadable/Rechargable weapon
- Add automation for Bulky property

### Compatibility:
- Tested with FVTT version [0.7.7](https://foundryvtt.com/releases/0.7.7)
- Also tested with [Midi Quality of Life Improvements](https://foundryvtt.com/packages/midi-qol/)

### Usage notes:
- Most worn items granting passive effects will not do so unless equipped, and some (e.g., Shield Bracers) must then also be activated.
- PC-sheet actors created prior to enabling this module will likely exhibit undocumented behavior. They should be recreated.

### Author
- Shark that walks like a man

### Feedback
If you have any suggestions or feedback, please make an issue report or contact me on Discord (stwlam#3718).
