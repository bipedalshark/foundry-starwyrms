import ActorSS from "./actor/entity.js";
import ItemSS from "./item/entity.js";
import ActorSheetSSNPC from "./actor/sheets/npc.js";
import ActorSheetSSCharacter from "./actor/sheets/character.js";
import ActorSheet5eNPC from "/systems/dnd5e/module/actor/sheets/npc.js";
import ActorSheet5eCharacter from "/systems/dnd5e/module/actor/sheets/character.js";


const sortObjectVals = (unsorted) => {
  return Object.keys(unsorted).sort(
    (key1, key2) => unsorted[key1] > unsorted[key2] ? 1 : -1
  ).reduce((sorted, key) => {
    sorted[key] = unsorted[key];
    return sorted;
  }, { });
};

export const currenciesToDelete = ["pp", "ep", "sp", "cp"];
export const skillsToDelete = ["ani", "nat", "prf", "rel", "slt"];

export function apply () {
  // Use module entity classes
  CONFIG.Actor.entityClass = ActorSS;
  CONFIG.Item.entityClass = ItemSS;

  game.i18n.translations.DND5E.Charges = "Uses";
  CONFIG.DND5E.limitedUsePeriods.charges = "Item Uses";

  CONFIG.DND5E.damageTypes.ballistic = "Ballistic";
  CONFIG.DND5E.damageTypes.plasma = "Plasma";
  CONFIG.DND5E.damageTypes = sortObjectVals(CONFIG.DND5E.damageTypes);

  CONFIG.DND5E.damageResistanceTypes.ballistic = "Ballistic";
  CONFIG.DND5E.damageResistanceTypes.plasma = "Plasma";
  CONFIG.DND5E.damageResistanceTypes = sortObjectVals(CONFIG.DND5E.damageResistanceTypes);

  for (const skill of skillsToDelete) {
    delete CONFIG.DND5E.skills[skill];
  }
  CONFIG.DND5E.skills.cpt = "Computers";
  CONFIG.DND5E.skills.etq = "Etiquette";
  CONFIG.DND5E.skills.mch = "Mechanics";
  CONFIG.DND5E.skills.mdc = "Medicine";
  CONFIG.DND5E.skills.plt = "Piloting";
  CONFIG.DND5E.skills.sci = "Science";
  CONFIG.DND5E.skills = sortObjectVals(CONFIG.DND5E.skills);

  CONFIG.DND5E.weaponProperties["atm"] = "Automatic";
  CONFIG.DND5E.weaponProperties["blk"] = "Bulky";
  CONFIG.DND5E.weaponProperties["dbs"] = "Doubleshot";
  CONFIG.DND5E.weaponProperties["rds"] = "Radius";
  CONFIG.DND5E.weaponProperties["rmp"] = "Rampant";
  CONFIG.DND5E.weaponProperties["rcg"] = "Recharge";
  CONFIG.DND5E.weaponProperties["scp"] = "Scope";
  CONFIG.DND5E.weaponProperties["tre"] = "Timed Recharge";
  CONFIG.DND5E.weaponProperties = sortObjectVals(CONFIG.DND5E.weaponProperties);
  CONFIG.DND5E.ResourceSecondary = "Resource 1";
  CONFIG.DND5E.ResourceTertiary = "Resource 2";

  CONFIG.STARWYRMS = {
    armorProperties: {
      rbd: "Resist Ballistic Damage",
      spb: "Spaceable",
      spc: "Special",
      thr: "Thrusters"
    },
    shieldProperties: {
      atv: "Activated",
      cvr: "Cover"
    }
  };

  CONFIG.DND5E.currencies = { gp: "Credits" };
  CONFIG.DND5E.currencyConversions = { };

  // Override 5E NPC Actor Sheet class
  Actors.unregisterSheet("dnd5e", ActorSheet5eNPC);
  Actors.registerSheet("starwyrms", ActorSheetSSNPC, {
    types: ["npc"],
    makeDefault: true,
    label: "Starwyrms.SheetClassNPC"
  });
  Actors.unregisterSheet("dnd5e", ActorSheet5eCharacter);
  Actors.registerSheet("starwyrms", ActorSheetSSCharacter, {
    types: ["character"],
    makeDefault: true,
    label: "Starwyrms.SheetClassCharacter"
  });
}


// Extend Token class to show/hide Energy Barrier effect

Token.prototype.updateBarrierEffect = async function (bpValue) {
  const lightKeys = ["brightLight", "lightAngle", "lightColor", "lightAlpha", "lightAnimation"];
  const oldLight = this.data.flags?.starwyrms?.oldLight;
  if (bpValue > 0 && this.data.flags?.starwyrms?.oldLight === undefined) {
    await this.setFlag(
      "starwyrms", "oldLight",
      Object.keys(this.data).reduce(
        (lightData, key) => {
          if (lightKeys.includes(key)) {
            lightData[key] = this.data[key];
          }
          return lightData;
        }, { }
      )
    );
    await this.update({
      "brightLight": 0.35,
      "lightAngle":360,
      "lightColor": "#35CEE1",
      "lightAlpha":0.35,
      "lightAnimation": {
        "type": "hexa",
        "speed": 3,
        "intensity": 4
      }
    });
  } else if ([0, null].includes(bpValue) && oldLight !== undefined) {
    await this.update(this.data.flags.starwyrms.oldLight);
    await this.unsetFlag("starwyrms", "oldLight");
  }
};
