import * as hooks from "./hooks.js";

class Starwyrms extends Application {
  static start() {
    return new this();
  }

  constructor() {
    super();
    hooks.register();
  }
}

Starwyrms.start();
