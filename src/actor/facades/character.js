export default class Character {
  constructor(actorEntity) {
    this.entity = actorEntity;
    this.abilities = Object.keys(actorEntity.data.data.abilities).map(
      (key) => new Ability(actorEntity, key)
    ).reduce((abilities, ability) => {
      abilities[ability._key] = ability;
      return abilities;
    }, { });
  }

  get tokenEntity() {
    return canvas.tokens.placeables.find((token) => token.actorId = this.entity.id);
  }

  get name() {
    return this.entity.name;
  }

  get inventory() {
    return Array.from(this.entity.items).map((item) => item.facade);
  }

  getItemById(itemId) {
    return this.entity.items.get(itemId).facade;
  }

  getItemsBySubtype(subtype) {
    return Array.from(this.entity.items).filter(
      (entity) => entity.data.flags?.starwyrms?.subtype === subtype
    ).map((entity) => entity.facade);
  }

  getItemsByName(name) {
    return Array.from(this.entity.items).filter(
      (itemEntity) => itemEntity.name === name
    ).map((itemEntity) => itemEntity.facade);
  }

  get barrierPoints() {
    return this.entity.data.data.resources.primary?.value || 0;
  }

  async setBarrierPoints(newValue, newMax=undefined) {
    await this.entity.update({"data.resources.primary.value": newValue});
    if (newMax !== undefined) {
      await this.entity.update({"data.resources.primary.max": newMax});
    }
  }

  async equip(item, options) {
    return await item.setIsEquipped(true, options);
  }

  async unequip(item) {
    return await item.setIsEquipped(false);
  }
}

class Ability {
  constructor(actorEntity, key) {
    this.entity = actorEntity;
    this._key = key;
    this.name = {
      str: "Strength",
      dex: "Dexterity",
      con: "Constitution",
      int: "Intelligence",
      wis: "Wisdom",
      cha: "Charisma"
    }[key];
  }

  get value() {
    return this.entity.data.data.abilities[this._key].value;
  }

  async setValue(newValue) {
    await this.entity.update({[`data.abilities.${this._key}.value`]: newValue});
  }

  get modifier() {
    return this.entity.data.data.abilities[this._key].mod;
  }
}
