import Actor5e from "/systems/dnd5e/module/actor/entity.js";
import Character from "./facades/character.js";
import {currenciesToDelete, skillsToDelete} from "../reconfig.js";


class ActorSS extends Actor5e {

  get facade() {
    if (this._facade instanceof Character) {
      return this._facade;
    }

    this._facade = new Character(this);
    return this._facade;
  }

  static async create(data, options) {
    const primaryResource = { label: "Barrier Points", value: 0, max: 0 };
    if (data.type === "vehicle") {
      data.resources = { primary: primaryResource };
      return super.create(data, options);
    }

    const actor = await super.create(data, options);
    const actorData = actor.data;

    actorData.data.resources.primary = primaryResource;

    const skillsToAbilities = {
      cpt: "int",
      etq: "cha",
      mch: "wis",
      med: "int",
      plt: "dex",
      sci: "int"
    };

    // Add new skills
    for (const skill in skillsToAbilities) {
      actorData.data.skills[skill] = {
        value: 0,
        ability: skillsToAbilities[skill],
        bonus: 0,
        mod: 0,
        prof: 0,
        total: 0,
        passive: 10
      };
    }

    for (const skill of skillsToDelete) {
      delete actorData.data.skills[skill];
    }
    // Sort remaining skills
    actorData.skills = Object.keys(actorData.data.skills).sort(
      (key1, key2) => CONFIG.DND5E.skills[key1] > CONFIG.DND5E.skills[key2] ? 1 : -1
    ).reduce((skills, key) => {
      skills[key] = actorData.data.skills[key];
      return skills;
    }, { });

    const currencyDeletions = currenciesToDelete.map((currency) =>
      [`data.currency.-=${currency}`, null]);
    const skillDeletions = skillsToDelete.map((skill) => [`data.skills.-=${skill}`, null]);
    const deletions = Object.fromEntries(currencyDeletions.concat(skillDeletions));
    const updates = { data: actorData.data, ...deletions };

    await actor.update(updates);

    return actor;
  }
}

export default ActorSS;
