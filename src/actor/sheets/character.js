import ActorSheet5eCharacter from "/systems/dnd5e/module/actor/sheets/character.js";

export default class ActorSheetSSCharacter extends ActorSheet5eCharacter {
  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const character = this.actor.facade;
    const item = character.getItemById(itemId);
    if (item.type === "spell") {
      super._onToggleItem(event);
      return;
    }

    // Take over DnD5e dnd5e.equipped Toggle
    const $button = $(event.delegateTarget);
    if (!$button.hasClass("active")) {
      character.equip(item, { notify: true });
    } else {
      character.unequip(item);
    }
  }
}
