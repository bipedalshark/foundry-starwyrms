import ActorSheet5eNPC from "/systems/dnd5e/module/actor/sheets/npc.js";

export default class ActorSheetSSNPC extends ActorSheet5eNPC {
  get template() {
    return "/modules/starwyrms/templates/actors/npc-sheet.html";
  }
}
