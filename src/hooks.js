import * as reconfig from "./reconfig.js";

export const register = () => {
  Hooks.once("setup", async () => {
    reconfig.apply();

    const selectors = [
      "#chat-log li.context-item",
      "#chat-log span.dmgBtn-container-mqol button"
    ].join(", ");
    const itemSubtype = (itemData) => itemData.flags?.starwyrms?.subtype;

    $(document).on("mouseover", selectors, async function (event) {
      const $option = $(event.target);

      // Go by .text() if in context menu, .hasClass(...) if clicking midi-qol buttons
      const dmgMultiplierIndicators = {
        full: { text: "Apply Damage", class: "dice-total-full-damage-button" },
        double: { text: "Apply Double Damage", class: "dice-total-double-damage-button" },
        half: { text: "Apply Half Damage", class: "dice-total-half-damage-button" },
      };
      const dmgMultiplierKeys = Object.keys(dmgMultiplierIndicators);
      const dmgMultiplierKey = dmgMultiplierKeys.find((key) => {
        const text = dmgMultiplierIndicators[key].text;
        const className = dmgMultiplierIndicators[key].class;
        return text === $option.text() || $option.hasClass(className);
      });

      if (dmgMultiplierKey !== undefined) {
        $option.off("click");

        const $message = $(event.target).parents("li.message");
        $option.on("click", async (clickEvent) => {
          clickEvent.stopPropagation();

          const dmgMultiplier = {full: 1, double: 2, half: 0.5}[dmgMultiplierKey];
          const damage = {
            amount: Math.ceil(Number($message.find("h4.dice-total").text()) * dmgMultiplier),
            type: $message.find("span.flavor-text").text().match(/\((\w+)\)$/)[1]
          };
          console.debug(`Applying ${damage.amount} (${dmgMultiplierKey}) ${damage.type} damage`);

          for ( const token of canvas.tokens.controlled ) {
            const actor = token.actor;
            const barrierPoints = actor.data.data.resources.primary.value;
            const barrierDamage = ["Ballistic", "Plasma"].includes(damage.type)
                  ? Math.min(barrierPoints, damage.amount)
                  : 0;
            const newBarrier = Math.max(0, barrierPoints - barrierDamage);
            const healthDamage = Math.max(0, damage.amount - barrierDamage);

            const character = actor.facade;
            const bracer = character.getItemsBySubtype("bracer")[0];
            if (bracer && bracer.isActive && newBarrier < bracer.charges.value) {
              await bracer.setCharges(newBarrier);
            } else {
              await actor.update({"data.resources.primary.value": newBarrier});
            }

            const resistances = [
              ...actor.data.data.traits.dr.value,
              ...actor.data.data.traits.dr.custom.toLowerCase().split(";")
            ].filter((resistance) => resistance !== "");
            if (resistances.includes(damage.type.toLowerCase())) {
              const resistedDamage = healthDamage - Math.floor(healthDamage / 2);
              ui.notifications.info(
                `${actor.name} resisted ${resistedDamage} ${damage.type} damage.`
              );
              await actor.applyDamage(healthDamage - resistedDamage);
            } else {
              await actor.applyDamage(healthDamage);
            }
          }
        });
      }
    });

    // Combine ammunition stacks upon adding a new stack to an actor
    Hooks.on("preCreateOwnedItem", (actor, itemData, options) => {
      const subtype = itemSubtype(itemData);
      if (itemData.type === "consumable" && ["bullets", "battery"].includes(subtype)) {
        const existingStack = actor.facade.inventory.find(
          (item) => (item.type === "consumable"
                     && item.subtype === subtype
                     && item.name === itemData.name
                     && item.entity.id !== itemData._id)
        );
        if (existingStack !== undefined) {
          const newQuantity = existingStack.quantity + itemData.data.quantity;
          existingStack.setQuantity(newQuantity);
          options.temporary = true;

          return false;
        }
      }

      return true;
    });

    // Preconfigure use-limited weapons to consume from own charges
    Hooks.on("createOwnedItem", async (actor, itemData) => {
      const item = actor.facade.getItemById(itemData._id);
      if (item.type === "weapon" && ["ballistic", "energy"].includes(item.subtype)) {
        await item.entity.update({"data.consume.target": item.entity.id});

      }
    });

    // Activate passive item effects upon equipping; deactivate upon unequipping
    Hooks.on("updateOwnedItem", async (actor, itemData, diff) => {
      if (itemData.flags?.starwyrms === undefined) {
        return;
      }
      const character = actor.facade;
      const item = character.getItemById(itemData._id);
      if (item.subtype === "bracer" && typeof diff.data?.uses?.value === "number") {
        item.setCharges(diff.data.uses.value);
      }
    });
  });

  // Display Barrier Points effect on token
  Hooks.on("preUpdateActor", async (actor, diff) => {
    const isBpUpdate = diff.data?.resources?.primary !== undefined;
    if (!isBpUpdate) {
      return;
    }

    const tokens = canvas.tokens.placeables.filter((token) => token.actor?.id === actor.id);
    const bpValue = diff.data.resources.primary.value;
    for (const token of tokens) {
      token.updateBarrierEffect(bpValue);
    }
  });

  Hooks.on("preUpdateToken", async (_scene, tokenData, diff) => {
    const isBpUpdate = diff.actorData?.data?.resources?.primary !== undefined;
    if (!isBpUpdate) {
      return;
    }

    const tokens = canvas.tokens.placeables.filter(
      (tokenEntity) => tokenEntity.id === tokenData._id
    );
    const bpValue = diff.actorData.data.resources.primary.value;
    for (const token of tokens) {
      token.updateBarrierEffect(bpValue);
    }
  });

  Hooks.on("createToken", async (_scene, tokenData) => {
    const character = game.actors.get(tokenData.actorId).facade;
    const bpValue = character.barrierPoints;
    if ([0, null].includes(bpValue)) {
      return;
    }

    const tokens = canvas.tokens.placeables.filter((token) => token.id === tokenData._id);
    for (const token of tokens) {
      setTimeout(() => {
        token.updateBarrierEffect(bpValue);
      }, 100);
    }
  });
};
