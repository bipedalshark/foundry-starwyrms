import ItemFacade from "./base.js";

class Bracer extends ItemFacade {
  constructor(foundryItem) {
    super(foundryItem);
    this.maxEquipped = 1;
  }

  get charges() {
    return this.entity.data.data.uses;
  }

  async setCharges(newValue) {
    console.debug(`Setting charges to ${newValue}`);

    await this.entity.update({"data.uses.value": newValue});

    const changes = this.entity.transferredEffects[0].data.changes;
    changes[0].value = newValue;
    await this.entity.transferredEffects[0].update({"changes": changes});

    if (this.isActive) {
      if (this.hasCharges) {
        await this.owner.setBarrierPoints(this.charges.value);
      } else {
        await this.deactivate();
      }
    }
  }

  get hasCharges() {
    return this.charges.value > 0;
  }

  get isActive() {
    return !this.entity.transferredEffects[0].data.disabled;
  }

  async activate() {
    if (!this.isActive) {
      console.debug("Activating bracer");
      await this.entity.transferredEffects[0].update({"disabled": false});
      await this.owner.setBarrierPoints(this.charges.value, this.charges.max);
    }
  }

  async deactivate() {
    if (this.isActive) {
      console.debug("Deactivating bracer");
      await this.entity.transferredEffects[0].update({"disabled": true});
      await this.owner.setBarrierPoints(null, null);
    }
  }

  async recharge() {
    console.debug("Recharging bracer");
    await this.setCharges(this.charges.max);
  }

  async setIsEquipped(newValue) {
    if (newValue === false) {
      await this.deactivate();
    } else {
      this.setCharges(this.charges.value);
    }
    await super.setIsEquipped(newValue);
  }
}

export default Bracer;
