import ItemFacade from "./base.js";

export default class Weapon extends ItemFacade {
  constructor(itemEntity) {
    super(itemEntity);
    this.maxEquipped = itemEntity.data.data.properties.two ? 1 : 2;
  }
}
