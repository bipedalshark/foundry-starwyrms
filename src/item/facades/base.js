export default class ItemFacade {
  constructor(itemEntity) {
    this.entity = itemEntity;
    this.entitySourceId = itemEntity.data?.flags?.core?.sourceId;
    this.isFromStarwyrms = itemEntity.data.flags?.starwyrms !== undefined;
    this.maxEquipped = null; // If not null, max N of this item subtype equipped on an actor
  }

  get name() {
    return this.entity.name;
  }

  get type() {
    return this.entity.type;
  }

  get subtype() {
    return this.entity.data.flags?.starwyrms?.subtype;
  }

  get owner() {
    if (this._owner) {
      return this._owner;
    } else {
      this._owner = this.entity.actor.facade;
      return this._owner;
    }
  }

  get hasEffectOnEquip() {
    return (this.entity.data.data.activation.type === ""
            && this.entity.transferredEffects.length > 0);
  }

  get isEquipped() {
    return this.entity.data.data.equipped === true;
  }

  get uses() {
    return this.entity.data.data.uses;
  }

  async setUses(newValue) {
    this.entity.update({"data.uses.value": newValue});
  }

  async consumeUses(amount) {
    const remaining = this.uses.value  >= amount ? this.uses.value - amount : 0;
    await this.entity.update({"data.uses.value": remaining});
  }

  async setIsEquipped(newValue, options={ notify: false }) {
    if (this.isEquipped === newValue) {
      const equipped = newValue === true ? "equipped" : "unequipped";
      console.debug(`DEBUG: Item (${this.entity.id}) is already ${equipped}`);
    } else {
      await this.entity.update({"data.equipped": newValue});
    }

    const runNum = parseInt(Math.random() * 100000000);
    if (newValue === true) {
      console.log(`${runNum}: Equipping ${this.name}`);
      if (options.notify) {
        const $html = await renderTemplate(
          `/modules/starwyrms/templates/chat/item-equip-card.html`,
          { character: this.owner, token: this.owner.tokenEntity, item: this }
        );
        ChatMessage.create({
          user: game.user._id,
          type: CONST.CHAT_MESSAGE_TYPES.OTHER,
          content: $html,
          flavor: `${this.name} - Equip`,
          speaker: { actor: this.owner, token: this.tokenEntity, alias: this.owner.name },
          flags: {"core.canPopout": true}
        });
      }
    } else {
      console.log(`${runNum}: Unequipping ${this.name}`);
    }
    const otherEquipped = this.owner.getItemsBySubtype(this.subtype).filter(
      (item) => item.entity.id != this.entity.id && item.isEquipped
    );
    if (newValue === true && otherEquipped.length + 1 > this.maxEquipped) {
      // Unequip other items of same subtype
      otherEquipped.slice(
        0, otherEquipped.length + 1 - this.maxEquipped
      ).forEach(
        async (item) => {
          console.log(`${runNum}: ${item.entity.id}`);
          console.log(item);
          await item.setIsEquipped(false);
        }
      );
    }

    if (this.hasEffectOnEquip) {
      for (const effect of this.entity.transferredEffects) {
        await effect.update({"disabled": !newValue});
      }
    }
  }

  get quantity() {
    return this.entity.data.data.quantity;
  }

  async setQuantity(newQuantity) {
    if (newQuantity === 0) {
      return this.entity.delete();
    }

    return this.entity.update({"data.quantity": newQuantity});
  }

  async consume(amount) {
    const newQuantity = this.quantity >= amount
          ? this.quantity - amount
          : 0;
    return this.setQuantity(newQuantity);
  }
}
