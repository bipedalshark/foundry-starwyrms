import ItemFacade from "./base.js";

export default class Armor extends ItemFacade {
  constructor(foundryItem) {
    super(foundryItem);
    this.maxEquipped = 1;
  }

  get armorClass() {
    return this.entity.data.data.armor.value;
  }

  get dexCap() {
    const storedValue = this.entity.data.data.armor.dex;
    return typeof storedValue === "number" ? storedValue : 6;
  }

  // Adjust AC of armor effect according to character's dex.modifier and the armor's dexCap
  async setIsEquipped(newValue) {
    await super.setIsEquipped(newValue);

    const dexMod = this.owner.abilities.dex.modifier <= this.dexCap
          ? this.owner.abilities.dex.modifier
          : this.dexCap;

    if (newValue === true) {
      const effects = this.entity.data.effects;
      const armorEffect = this.entity.data.effects.find(
        (effect) => effect.label === "Equipped Armor"
      );
      const effectChange = armorEffect.changes.find(
        (change) => change.key === "data.attributes.ac.value"
      );

      // Set AC value of ActiveEffect
      effectChange.value = this.armorClass + dexMod - 10;
      await this.entity.update({effects: effects});

      await this.entity.transferredEffects.find((effect) => {
        const entityId = this.entity.id;
        const actorId = this.entity.actor.id;
        return effect.data.origin === `Actor.${actorId}.OwnedItem.${entityId}`;
      }).update({changes: armorEffect.changes});
    } else {
      await this.owner.entity.update({"data.attributes.ac.value": 10 + dexMod});
   }
  }
}
