import Item5e from "/systems/dnd5e/module/item/entity.js";
import Armor from "./facades/armor.js";
import Bracer from "./facades/bracer.js";
import Weapon from "./facades/weapon.js";
import ItemFacade from "./facades/base.js";
import AmmoReloadDialog from "../apps/ammo-reload-dialog.js";
import WeaponReloadDialog from "../apps/weapon-reload-dialog.js";
import ItemRechargeDialog from "../apps/item-recharge-dialog.js";
import ItemEquipDialog from "../apps/item-equip-dialog.js";

class ItemSS extends Item5e {
  get subtype() {
    return this.data.flags?.starwyrms?.subtype;
  }

  get uses() {
    return this.data.data.uses;
  }

  get quantity() {
    return this.data.data.quantity;
  }

  get isEquipped() {
    return this.data.data.equipped === true;
  }

  get hasEffectOnEquip() {
    return (this.data.data.activation.type === ""
            && this.transferredEffects.length > 0);
  }

  get facade() {
    if (this._facade instanceof ItemFacade) {
      return this._facade;
    }

    switch (this.data.flags?.starwyrms?.subtype) {
    case "armor":
      this._facade = new Armor(this);
      break;
    case "bracer":
      this._facade = new Bracer(this);
      break;
    case "ballistic":
      this._facade = new Weapon(this);
      break;
    case "energy":
      this._facade = new Weapon(this);
      break;
    default:
      this._facade = new ItemFacade(this);
    }

    return this._facade;
  }


  /**
   *  @override
   */
  async roll(options={ }) {
    if (!this.isEquipped && (this.type === "weapon" || this.subtype === "bracer")) {
      const form = await ItemEquipDialog.create(this.facade);
      return await form === null ? null : this.facade.owner.equip(this.facade, { notify: true });
    }

    if (this.subtype === undefined) {
      return super.roll(options);
    }

    return await ({
      armor: () => super.roll(options),
      ballistic: () => this._rollLimitedUsage(),
      battery: () => this._rollAmmo(),
      bracer: () => this._rollBracer(),
      clickseal: () => super.roll(options),
      thrown: async () => await this._rollThrownWeapon(options),
      energy: () => this._rollLimitedUsage(),
      primitive: () => super.roll(options),
      bullets: () => this._rollAmmo(),
      shield: () => super.roll(options),
    })[this.subtype].call();
  }

  async rollAttack(options={ }) {
    const roll = await super.rollAttack(options);
    if (roll !== null) {
      if (["energy", "ballistic"].includes(this.subtype)) {
        await this.facade.consumeUses(1);
      } else if (["thrown"].includes(this.subtype)) {
        await this.delete();
      }
    }
  }


  /**
   *  @override
   */
  async _handleResourceConsumption({isCard=false, isAttack=false}={ }) {
    if (isCard && ["ballistic", "energy", "thrown"].includes(this.subtype)) {
      // Prevent dnd5e from consuming ammunition before attacking
      super._handleResourceConsumption({isCard: false, isAttack: isAttack});
    } else {
      super._handleResourceConsumption({isCard: isCard, isAttack: isAttack});
    }
  }

  async _rollLimitedUsage() {
    const item = this.facade;
    if (item.uses.value === 0) {
      const dialog = item.subtype === "ballistic" ? WeaponReloadDialog : ItemRechargeDialog;
      const form = await dialog.create(item);
      // Form pre-validation failed or player exited dialog
      if (form === null || !form.has("item-id")) {
        return;
      }

      const ammo = item.owner.getItemById(form.get("ammo-id"));
      await this._reloadRecharge(item, ammo);
    } else {
      super.roll();
    }
  }

  async _rollAmmo () {
    const ammo = this.facade;
    const form = await AmmoReloadDialog.create(ammo);

    // Form pre-validation failed or player exited dialog
    if (form === null || form.get("weapon-id") === null) {
      return;
    }

    const weapon = ammo.owner.getItemById(form.get("weapon-id"));
    if (weapon.uses.value < weapon.uses.max) {
      await this._reloadRecharge(weapon, ammo);
    }
  }

  async _rollBracer () {
    const bracer = this.facade;
    if (bracer.isActive) {
      await bracer.deactivate();
    } else if (!bracer.isActive && bracer.hasCharges) {
      await bracer.activate();
    } else {
      await this._rollLimitedUsage();
    }
  }

  async _reloadRecharge(item, ammo) {
    if (ammo.subtype === "bullets") {
      // Reload weapon with selected ammunition
      const ammoToConsume = ammo.quantity <= item.uses.max - item.uses.value
            ? ammo.quantity
            : item.uses.max - item.uses.value;

      await ammo.consume(ammoToConsume);
      await item.setUses(item.uses.value + ammoToConsume);
    } else {  // ammoSubtype === "battery"
      await ammo.consume(1);
      if (item.subtype === "bracer") {
        await item.recharge();
      } else {
        await item.setUses(item.uses.max);;
      }
    }

    const character = item.owner;
    const action = ammo.subtype === "bullets" ? "Reload" : "Recharge";

    const ammoDetails = (() => {
      const ammoUuid = ammo.entitySourceId.replace(/^Compendium\./, "");
      return {
        type: ammo.subtype === "bullets" ? "Ammunition" : "Energy Source",
        link: `@Compendium[${ammoUuid}]{${ammo.name}}`,
        actionType: ammo.subtype === "bullets" ? "1 Bonus Action" : "1 Action"
      };
    })();

    const token = character.tokenEntity;
    const html = await renderTemplate(
      `/modules/starwyrms/templates/chat/reload-recharge_card.html`,
      { character: character, token: token, item: item,
        ammo: ammo, ammoDetails: ammoDetails }
    );
    ChatMessage.create({
      user: game.user._id,
      type: CONST.CHAT_MESSAGE_TYPES.OTHER,
      content: html,
      flavor: `${item.name} - ${action}`,
      speaker: { actor: this.actor, token: token, alias: character.name },
      flags: {"core.canPopout": true}
    });
  }

  async _rollThrownWeapon(options) {
    const message = await super.roll(options);
    if (message instanceof ChatMessage) {
      await message.update({"flags.dnd5e": {
        itemData: this.data,
        roll: { type: null }
      }});
    } else if (typeof message === "object") {
      // super.roll sent unboxed message data instead of a ChatMessage
      message.flags["dnd5e"]["itemData"] = this.data;
      message.flags["dnd5e"]["roll"] = { type: null };
    }

    return message;
  }
}

export default ItemSS;
