export default class ItemEquipDialog extends Dialog {
  constructor(item, dialogData={}, options={}) {
    super(dialogData, options);
    this.options.classes = ["dnd5e", "dialog"];
    this.item = item;
  }

  static async create(item) {
    // Prepare data
    const templatePath = "modules/starwyrms/templates/dialog/item-equip.html";
    const html = await renderTemplate(templatePath, { item: item });

    // Create the Dialog and return as a Promise
    return new Promise((resolve) => {
      const dialog = new this(item, {
        title: "Equip Item",
        content: html,
        buttons: {
          equip: {
            icon: '<i class="fas fa-hand-rock"></i>',
            label: "OK",
            callback: (html) => resolve(new FormData(html[0].querySelector("form")))
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => resolve(null)
          }
        },
        default: "equip",
        close: () => resolve(null)
      });
      dialog.render(true);
    });
  }
}
