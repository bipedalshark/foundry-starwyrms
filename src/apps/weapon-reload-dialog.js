export default class WeaponReloadDialog extends Dialog {
  constructor(weapon, dialogData={}, options={}) {
    super(dialogData, options);
    this.options.classes = ["dnd5e", "dialog"];
    this.item = weapon;
  }

  static async create(weapon) {
    if (!weapon.owner) {
      throw new Error(`${weapon.name} (${weapon.entity.id}) has no owner`);
    }

    // Prepare data
    const ammos = weapon.owner.getItemsBySubtype("bullets").filter((item) => item.quantity > 0);

    if (ammos.length === 0) {
      ui.notifications.warn(`${weapon.owner.name} has no Ballistic Rounds in their inventory.`);
      return null;
    }

    const templatePath = "modules/starwyrms/templates/dialog/weapon-reload.html";
    const formData = {
      weapon: weapon,
      prompt: "Select a type of Ballistic Round.",
      ammos: ammos
    };
    const html = await renderTemplate(templatePath, formData);

    // Create the Dialog and return as a Promise
    return new Promise((resolve) => {
      const dialog = new this(weapon, {
        title: "Reload Ballistic Weapon",
        content: html,
        buttons: {
          reload: {
            icon: '<i class="fas fa-redo"></i>',
            label: "Reload",
            callback: (html) => resolve(new FormData(html[0].querySelector("form")))
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => resolve(null)
          }
        },
        default: "reload",
        close: () => resolve(null)
      });
      dialog.render(true);
    });
  }

}
