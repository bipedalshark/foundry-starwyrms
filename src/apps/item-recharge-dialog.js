export default class ItemRechargeDialog extends Dialog {
  constructor(item, dialogData={}, options={}) {
    super(dialogData, options);
    this.options.classes = ["dnd5e", "dialog"];
    this.item = item;
  }

  static async create(item) {
    // Prepare data
    const owner = item.owner;
    const batteries = owner.getItemsBySubtype("battery")[0];

    if (batteries === undefined || batteries.quantity === 0) {
      ui.notifications.warn(`${owner.name} has no Expendable Plasma Batteries.`);
      return null;
    }

    const templatePath = "modules/starwyrms/templates/dialog/item-recharge.html";
    const formData = {
      item: item,
      energySources: [batteries]
    };
    const html = await renderTemplate(templatePath, formData);

    // Create the Dialog and return as a Promise
    return new Promise((resolve) => {
      const dialog = new this(item, {
        title: "Recharge Energy Item",
        content: html,
        buttons: {
          recharge: {
            icon: '<i class="fas fa-redo"></i>',
            label: "Recharge",
            callback: (html) => resolve(new FormData(html[0].querySelector("form")))
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => resolve(null)
          }
        },
        default: "recharge",
        close: () => resolve(null)
      });
      dialog.render(true);
    });
  }
}
