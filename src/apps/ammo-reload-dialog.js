export default class AmmoReloadDialog extends Dialog {
  constructor(ammo, dialogData={}, options={}) {
    super(dialogData, options);
    this.options.classes = ["dnd5e", "dialog"];
    this.item = ammo;
  }

  static async create(ammo) {
    if (!ammo.entity.isOwned) {
      throw new Error(`${ammo.name} (${ammo.entity.id}) has no owner`);
    }

    const character = ammo.owner;
    const weapons = ammo.subtype === "bullets"
          ? character.getItemsBySubtype("ballistic")
          : character.getItemsBySubtype("energy").concat(character.getItemsBySubtype("bracer"));

    const quantity = ammo.quantity;
    const inflect = ammo.subtype === "bullets"
          ? { article: "a", noun: "Ballistic Weapon", verb: "Reload", uses: "Rounds" }
          : { article: "an", noun: "Energy Item", verb: "Recharge", uses: "Charges" };

    if (quantity === 0) {
      ui.notifications.error(`${character.name} is out of ${ammo.name}.`);
      return null;
    }
    if (weapons.length === 0) {
      ui.notifications.error(
        `${character.name} has no ${inflect.noun}s to ${inflect.verb.toLowerCase()}.`
      );
      return null;
    } else if (["energy", "bracer"].includes(weapons[0].subtype)) {
      if (weapons.filter(
        (item) => item.uses.value < item.uses.max
      ).length === 0) {
        ui.notifications.warn(`${character.name}'s Energy Items are all fully charged.`);
        return null;
      }
    }

    // Dialog form data
    const formData = {
      ammo: ammo,
      prompt: `Select ${inflect.article} ${inflect.noun} to ${inflect.verb.toLowerCase()}.`,
      weapons: weapons,
      inflect: inflect
    };
    const templatePath = "modules/starwyrms/templates/dialog/ammo-reload.html";
    const html = await renderTemplate(templatePath, formData);

    // Create the Dialog and return as a Promise
    return new Promise((resolve) => {
      const dialog = new this(ammo, {
        title: `${inflect.verb} ${inflect.noun}`,
        content: html,
        buttons: {
          reload: {
            icon: '<i class="fas fa-redo"></i>',
            label: `${inflect.verb} ${inflect.noun}`,
            callback: (html) => resolve(new FormData(html[0].querySelector("form")))
          },
          cancel: {
            icon: '<i class="fas fa-times"></i>',
            label: "Cancel",
            callback: () => resolve(null)
          }
        },
        default: "reload",
        close: () => resolve(null)
      });
      dialog.render(true);
    });
  }
}
